# TypeScript Next.js Apollo starter

This is a really simple project that show the usage of Next.js with TypeScript.

## How to use it?

### Using `create-next-app`

Execute [`create-next-app`](https://github.com/segmentio/create-next-app) with [Yarn](https://yarnpkg.com/lang/en/docs/cli/create/) or [npx](https://github.com/zkat/npx#readme) to bootstrap the example:

```bash
npx create-next-app --example with-typescript with-typescript-app
# or
yarn create next-app --example with-typescript with-typescript-app
```

## Setting up

- install dependencies: `npm install`
- copy `.env.example` into `.env` and edit for desired run configuration
- run `npm run gql:gen` to generate GraphQL stuff

## Environments

| Variable | Description        |    Default    |
| -------- | :----------------- | :-----------: |
| NODE_ENV | Environment        | `development` |
| PORT     | Port               |    `3000`     |
| API_URI  | Uri to GraphQL Api |               |

## Commands

Following commands expect that desired node.js version is set as default version used system-wide.

### Install dependencies:

```bash
$ npm install
```

### Build production:

```
$ npm run build
```

### Run production server:

```
$ npm start
```

### Run dev server:

```
$ npm run dev
```
