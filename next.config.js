const withCSS = require("@zeit/next-css");
const withTypescript = require("@zeit/next-typescript");
const webpack = require("webpack");
const dotenv = require("dotenv")

dotenv.config()

module.exports = withTypescript(
  withCSS({
    webpack: config => {
      config.plugins.push(
        new webpack.EnvironmentPlugin({
          API_URI: process.env.API_URI
        })
      );

      config.devtool = "source-map";

      return config;
    }
  }),
);