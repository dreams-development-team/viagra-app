import "antd/dist/antd.css";

export const globalStyles = {
  "html, body": {
    margin: 0,
    padding: 0,
    fontSize: "100%",
    minHeight: "100vh",
    maxWidth: "100vw",
    color: "#111111",
    fontFamily: ["Raleway"],
    backgroundColor: "#FFFCFC",
    letterSpacing: "1.3px",
    fontWeight: 300
  },

  "#__next": {
    minHeight: "100vh",

    "& *": {
      fontSize: "1.2rem"
    }
  }
};
