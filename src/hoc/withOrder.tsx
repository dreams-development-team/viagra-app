import React from "react";
import { orderStore } from "src/store/order";
import { Subscribe } from "unstated";

const withOrder = WrappedComponent => {
  return class WithOrder extends React.Component {
    render() {
      return (
        <Subscribe to={[orderStore]}>
          {order => <WrappedComponent order={order} {...this.props} />}
        </Subscribe>
      );
    }
  };
};

export default withOrder;
