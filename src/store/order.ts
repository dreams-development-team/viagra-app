import equal from "fast-deep-equal";
import cookies from "js-cookie";
import { Omit } from "lodash";
import { CartInput } from "src/components/apollo-components";
import { Container } from "unstated";

export type OrderState = {
  cart: CartInput[];
};

export type CartPayload = Omit<CartInput, "quantity" | "id">;

export class OrderStore extends Container<OrderState> {
  constructor(props: OrderState | undefined) {
    super();

    this.state = props || { cart: [] };
  }

  addToCart(payload: CartPayload) {
    this.setState(prevState => {
      const cart = [...prevState.cart];
      const detailIndex = cart.findIndex(
        ({ productId, dosageDetailId, dosageId }) =>
          equal(
            { productId, dosageDetailId, dosageId },
            {
              productId: payload.productId,
              dosageId: payload.dosageId,
              dosageDetailId: payload.dosageDetailId
            }
          )
      );

      if (detailIndex > -1) {
        cart[detailIndex].quantity += 1;
      } else {
        cart.push({ ...payload, quantity: 1 });
      }

      cookies.set(
        "order",
        {
          ...prevState,
          cart
        },
        { expires: 7 }
      );

      return {
        ...prevState,
        cart
      };
    }).catch();
  }

  setQuantity(payload: Omit<CartInput, "id">) {
    this.setState(prevState => {
      const cart = [...prevState.cart];
      const detailIndex = cart.findIndex(
        ({ productId, dosageDetailId, dosageId }) =>
          equal(
            { productId, dosageDetailId, dosageId },
            {
              productId: payload.productId,
              dosageId: payload.dosageId,
              dosageDetailId: payload.dosageDetailId
            }
          )
      );

      if (detailIndex > -1) {
        cart[detailIndex].quantity = payload.quantity;
      }
      cookies.set(
        "order",
        {
          ...prevState,
          cart
        },
        { expires: 7 }
      );

      return {
        ...prevState,
        cart
      };
    }).catch();
  }

  removeFromCart(payload: CartPayload) {
    this.setState(prevState => {
      const newState = {
        ...prevState,
        cart: prevState.cart.filter(
          c =>
            !equal(
              {
                dosageId: c.dosageId,
                dosageDetailId: c.dosageDetailId,
                productId: c.productId
              },
              payload
            )
        )
      };

      cookies.set("order", newState, { expires: 7 });

      return newState;
    }).catch();
  }

  clear() {
    cookies.remove("order");
    this.setState({ cart: [] }).catch();
  }

  setInitialState(state: OrderState) {
    return this.setState(state);
  }
}

export const orderStore = new OrderStore(cookies.getJSON("order"));
