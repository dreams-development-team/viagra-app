import gql from "graphql-tag";

export const searchProductsQuery = gql`
  query SearchProducts($where: ProductsWhereInput, $skip: Float, $take: Float) {
    searchProducts(where: $where, skip: $skip, take: $take) {
      count
      data {
        id
        title
        url
        logo {
          uid
          name
          url
        }
        dosages {
          id
          dosage
          details {
            id
            count
            price
          }
        }
        gallery {
          uid
          name
          url
        }
        withAlcohol
        timeToStart
        workTime
        meta {
          key
          value
        }
        description
        seoTitle
        seoDescription
        seoKeywords
        createdAt
        updatedAt
      }
    }
  }
`;

export const productQuery = gql`
  query Product($where: ProductWhereInput!) {
    product(where: $where) {
      id
      title
      url
      logo {
        uid
        name
        url
      }
      dosages {
        id
        dosage
        details {
          id
          count
          price
        }
      }
      gallery {
        uid
        name
        url
      }
      withAlcohol
      timeToStart
      workTime
      meta {
        key
        value
      }
      description
      seoTitle
      seoDescription
      seoKeywords
      createdAt
      updatedAt
    }
  }
`;
