import gql from "graphql-tag";


export const createOrderMutation = gql`
  mutation CreateOrder($data: OrderAddInput!) {
    createOrder(data: $data) {
      id
      number
      deliveryType
      deliveryStatus
      paymentType
      paymentStatus
      customerName
      phone
      comment
      email
      createdAt
      updatedAt
      address {
        city
        street
        apartment
        postalCode
      }
      cart {
        id
        productId
        dosageId
        dosageDetailId
        quantity
        product {
          id
          title
          url description
          logo {
            url
          }
          dosages {
            id
            dosage
            details {
              id
              count
              price
            }
          }
        }
        dosage {
          id
          dosage
        }
        dosageDetail {
          id
          count
          price
        }
      }
    }
  }
`;
