import styled from "@emotion/styled";

export const Center = styled("div")({
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
  height: "100%"
});
