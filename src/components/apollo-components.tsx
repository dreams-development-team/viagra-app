export type Maybe<T> = T | null;

export interface OrdersWhereInput {
  deliveryType?: Maybe<string>;

  paymentType?: Maybe<string>;

  deliveryStatus?: Maybe<string>;

  paymentStatus?: Maybe<string>;

  customerName?: Maybe<string>;

  phone?: Maybe<string>;

  number?: Maybe<string>;
}

export interface OrderWhereInput {
  id?: Maybe<number>;

  number?: Maybe<string>;
}

export interface ProductWhereInput {
  id?: Maybe<number>;

  title?: Maybe<string>;

  url?: Maybe<string>;
}

export interface ProductsWhereInput {
  id?: Maybe<number[]>;

  title?: Maybe<string>;

  url?: Maybe<string>;
}

export interface OrderAddInput {
  deliveryType: DeliveryTypes;

  deliveryStatus: DeliveryStatus;

  paymentType: PaymentTypes;

  paymentStatus: PaymentStatus;

  customerName: string;

  email?: Maybe<string>;

  address: AddressInput;

  phone?: Maybe<string>;

  comment?: Maybe<string>;

  cart: CartInput[];
}

export interface AddressInput {
  city: string;

  street: string;

  apartment: string;

  postalCode: string;
}

export interface CartInput {
  id?: Maybe<number>;

  quantity: number;

  dosageId: number;

  productId: number;

  dosageDetailId: number;
}

export interface OrderUpdateInput {
  id: number;

  deliveryType?: Maybe<DeliveryTypes>;

  deliveryStatus?: Maybe<DeliveryStatus>;

  paymentType?: Maybe<PaymentTypes>;

  paymentStatus?: Maybe<PaymentStatus>;

  customerName?: Maybe<string>;

  email?: Maybe<string>;

  address?: Maybe<AddressInput>;

  phone?: Maybe<string>;

  comment?: Maybe<string>;

  cart?: Maybe<CartInput[]>;
}

export interface ProductAddInput {
  title: string;

  url: string;

  logo: FileInput;

  gallery?: Maybe<FileInput[]>;

  withAlcohol: boolean;

  timeToStart: string;

  workTime: string;

  meta?: Maybe<ProductMetaInput[]>;

  dosages: DosageInput[];

  description: string;

  seoTitle: string;

  seoDescription: string;

  seoKeywords: string[];
}

export interface FileInput {
  uid?: Maybe<string>;

  name?: Maybe<string>;

  url: string;
}

export interface ProductMetaInput {
  key: string;

  value: string;
}

export interface DosageInput {
  id?: Maybe<number>;

  dosage: string;

  productId?: Maybe<number>;

  details: DosageDetailInput[];
}

export interface DosageDetailInput {
  id?: Maybe<number>;

  count: number;

  price: number;

  dosageId?: Maybe<number>;
}

export interface ProductUpdateInput {
  id: number;

  title?: Maybe<string>;

  url?: Maybe<string>;

  logo?: Maybe<FileInput>;

  gallery?: Maybe<FileInput[]>;

  withAlcohol?: Maybe<boolean>;

  timeToStart?: Maybe<string>;

  workTime?: Maybe<string>;

  meta?: Maybe<ProductMetaInput[]>;

  dosages?: Maybe<DosageInput[]>;

  description?: Maybe<string>;

  seoTitle?: Maybe<string>;

  seoDescription?: Maybe<string>;

  seoKeywords?: Maybe<string[]>;
}
/** Order delivery types */
export enum DeliveryTypes {
  Courier = "courier",
  Mail = "mail",
  Pickup = "pickup"
}
/** Order delivery status */
export enum DeliveryStatus {
  Pending = "pending",
  InProgress = "inProgress",
  Confirm = "confirm",
  Shipped = "shipped",
  Done = "done"
}
/** Order payment types */
export enum PaymentTypes {
  Cash = "cash",
  Card = "card"
}
/** Order payment status */
export enum PaymentStatus {
  Pending = "pending",
  Paid = "paid",
  Cancel = "cancel"
}

/** The `Upload` scalar type represents a file upload. */
export type Upload = any;

// ====================================================
// Documents
// ====================================================

export type CreateOrderVariables = {
  data: OrderAddInput;
};

export type CreateOrderMutation = {
  __typename?: "Mutation";

  createOrder: CreateOrderCreateOrder;
};

export type CreateOrderCreateOrder = {
  __typename?: "OrderEntity";

  id: number;

  number: string;

  deliveryType: DeliveryTypes;

  deliveryStatus: DeliveryStatus;

  paymentType: PaymentTypes;

  paymentStatus: PaymentStatus;

  customerName: string;

  phone: Maybe<string>;

  comment: Maybe<string>;

  email: Maybe<string>;

  createdAt: string;

  updatedAt: string;

  address: CreateOrderAddress;

  cart: CreateOrderCart[];
};

export type CreateOrderAddress = {
  __typename?: "OrderAddress";

  city: string;

  street: string;

  apartment: string;

  postalCode: string;
};

export type CreateOrderCart = {
  __typename?: "CartEntity";

  id: number;

  productId: number;

  dosageId: number;

  dosageDetailId: number;

  quantity: number;

  product: CreateOrderProduct;

  dosage: CreateOrderDosage;

  dosageDetail: CreateOrderDosageDetail;
};

export type CreateOrderProduct = {
  __typename?: "ProductEntity";

  id: number;

  title: string;

  url: string;

  description: string;

  logo: CreateOrderLogo;

  dosages: CreateOrderDosages[];
};

export type CreateOrderLogo = {
  __typename?: "File";

  url: string;
};

export type CreateOrderDosages = {
  __typename?: "DosageEntity";

  id: number;

  dosage: string;

  details: CreateOrderDetails[];
};

export type CreateOrderDetails = {
  __typename?: "DosageDetailsEntity";

  id: number;

  count: number;

  price: number;
};

export type CreateOrderDosage = {
  __typename?: "DosageEntity";

  id: number;

  dosage: string;
};

export type CreateOrderDosageDetail = {
  __typename?: "DosageDetailsEntity";

  id: number;

  count: number;

  price: number;
};

export type SearchProductsVariables = {
  where?: Maybe<ProductsWhereInput>;
  skip?: Maybe<number>;
  take?: Maybe<number>;
};

export type SearchProductsQuery = {
  __typename?: "Query";

  searchProducts: SearchProductsSearchProducts;
};

export type SearchProductsSearchProducts = {
  __typename?: "ProductsPayload";

  count: number;

  data: SearchProductsData[];
};

export type SearchProductsData = {
  __typename?: "ProductEntity";

  id: number;

  title: string;

  url: string;

  logo: SearchProductsLogo;

  dosages: SearchProductsDosages[];

  gallery: Maybe<SearchProductsGallery[]>;

  withAlcohol: boolean;

  timeToStart: string;

  workTime: string;

  meta: Maybe<SearchProductsMeta[]>;

  description: string;

  seoTitle: string;

  seoDescription: string;

  seoKeywords: string[];

  createdAt: string;

  updatedAt: string;
};

export type SearchProductsLogo = {
  __typename?: "File";

  uid: Maybe<string>;

  name: Maybe<string>;

  url: string;
};

export type SearchProductsDosages = {
  __typename?: "DosageEntity";

  id: number;

  dosage: string;

  details: SearchProductsDetails[];
};

export type SearchProductsDetails = {
  __typename?: "DosageDetailsEntity";

  id: number;

  count: number;

  price: number;
};

export type SearchProductsGallery = {
  __typename?: "File";

  uid: Maybe<string>;

  name: Maybe<string>;

  url: string;
};

export type SearchProductsMeta = {
  __typename?: "ProductMeta";

  key: string;

  value: string;
};

export type ProductVariables = {
  where: ProductWhereInput;
};

export type ProductQuery = {
  __typename?: "Query";

  product: Maybe<ProductProduct>;
};

export type ProductProduct = {
  __typename?: "ProductEntity";

  id: number;

  title: string;

  url: string;

  logo: ProductLogo;

  dosages: ProductDosages[];

  gallery: Maybe<ProductGallery[]>;

  withAlcohol: boolean;

  timeToStart: string;

  workTime: string;

  meta: Maybe<ProductMeta[]>;

  description: string;

  seoTitle: string;

  seoDescription: string;

  seoKeywords: string[];

  createdAt: string;

  updatedAt: string;
};

export type ProductLogo = {
  __typename?: "File";

  uid: Maybe<string>;

  name: Maybe<string>;

  url: string;
};

export type ProductDosages = {
  __typename?: "DosageEntity";

  id: number;

  dosage: string;

  details: ProductDetails[];
};

export type ProductDetails = {
  __typename?: "DosageDetailsEntity";

  id: number;

  count: number;

  price: number;
};

export type ProductGallery = {
  __typename?: "File";

  uid: Maybe<string>;

  name: Maybe<string>;

  url: string;
};

export type ProductMeta = {
  __typename?: "ProductMeta";

  key: string;

  value: string;
};

import gql from "graphql-tag";
import * as React from "react";
import * as ReactApollo from "react-apollo";
import * as ReactApolloHooks from "react-apollo-hooks";

// ====================================================
// Components
// ====================================================

export const CreateOrderDocument = gql`
  mutation CreateOrder($data: OrderAddInput!) {
    createOrder(data: $data) {
      id
      number
      deliveryType
      deliveryStatus
      paymentType
      paymentStatus
      customerName
      phone
      comment
      email
      createdAt
      updatedAt
      address {
        city
        street
        apartment
        postalCode
      }
      cart {
        id
        productId
        dosageId
        dosageDetailId
        quantity
        product {
          id
          title
          url
          description
          logo {
            url
          }
          dosages {
            id
            dosage
            details {
              id
              count
              price
            }
          }
        }
        dosage {
          id
          dosage
        }
        dosageDetail {
          id
          count
          price
        }
      }
    }
  }
`;
export class CreateOrderComponent extends React.Component<
  Partial<ReactApollo.MutationProps<CreateOrderMutation, CreateOrderVariables>>
> {
  render() {
    return (
      <ReactApollo.Mutation<CreateOrderMutation, CreateOrderVariables>
        mutation={CreateOrderDocument}
        {...(this as any)["props"] as any}
      />
    );
  }
}
export type CreateOrderProps<TChildProps = any> = Partial<
  ReactApollo.MutateProps<CreateOrderMutation, CreateOrderVariables>
> &
  TChildProps;
export type CreateOrderMutationFn = ReactApollo.MutationFn<
  CreateOrderMutation,
  CreateOrderVariables
>;
export function CreateOrderHOC<TProps, TChildProps = any>(
  operationOptions:
    | ReactApollo.OperationOption<
        TProps,
        CreateOrderMutation,
        CreateOrderVariables,
        CreateOrderProps<TChildProps>
      >
    | undefined
) {
  return ReactApollo.graphql<
    TProps,
    CreateOrderMutation,
    CreateOrderVariables,
    CreateOrderProps<TChildProps>
  >(CreateOrderDocument, operationOptions);
}
export function useCreateOrder(
  baseOptions?: ReactApolloHooks.MutationHookOptions<
    CreateOrderMutation,
    CreateOrderVariables
  >
) {
  return ReactApolloHooks.useMutation<
    CreateOrderMutation,
    CreateOrderVariables
  >(CreateOrderDocument, baseOptions);
}
export const SearchProductsDocument = gql`
  query SearchProducts($where: ProductsWhereInput, $skip: Float, $take: Float) {
    searchProducts(where: $where, skip: $skip, take: $take) {
      count
      data {
        id
        title
        url
        logo {
          uid
          name
          url
        }
        dosages {
          id
          dosage
          details {
            id
            count
            price
          }
        }
        gallery {
          uid
          name
          url
        }
        withAlcohol
        timeToStart
        workTime
        meta {
          key
          value
        }
        description
        seoTitle
        seoDescription
        seoKeywords
        createdAt
        updatedAt
      }
    }
  }
`;
export class SearchProductsComponent extends React.Component<
  Partial<ReactApollo.QueryProps<SearchProductsQuery, SearchProductsVariables>>
> {
  render() {
    return (
      <ReactApollo.Query<SearchProductsQuery, SearchProductsVariables>
        query={SearchProductsDocument}
        {...(this as any)["props"] as any}
      />
    );
  }
}
export type SearchProductsProps<TChildProps = any> = Partial<
  ReactApollo.DataProps<SearchProductsQuery, SearchProductsVariables>
> &
  TChildProps;
export function SearchProductsHOC<TProps, TChildProps = any>(
  operationOptions:
    | ReactApollo.OperationOption<
        TProps,
        SearchProductsQuery,
        SearchProductsVariables,
        SearchProductsProps<TChildProps>
      >
    | undefined
) {
  return ReactApollo.graphql<
    TProps,
    SearchProductsQuery,
    SearchProductsVariables,
    SearchProductsProps<TChildProps>
  >(SearchProductsDocument, operationOptions);
}
export function useSearchProducts(
  baseOptions?: ReactApolloHooks.QueryHookOptions<SearchProductsVariables>
) {
  return ReactApolloHooks.useQuery<
    SearchProductsQuery,
    SearchProductsVariables
  >(SearchProductsDocument, baseOptions);
}
export const ProductDocument = gql`
  query Product($where: ProductWhereInput!) {
    product(where: $where) {
      id
      title
      url
      logo {
        uid
        name
        url
      }
      dosages {
        id
        dosage
        details {
          id
          count
          price
        }
      }
      gallery {
        uid
        name
        url
      }
      withAlcohol
      timeToStart
      workTime
      meta {
        key
        value
      }
      description
      seoTitle
      seoDescription
      seoKeywords
      createdAt
      updatedAt
    }
  }
`;
export class ProductComponent extends React.Component<
  Partial<ReactApollo.QueryProps<ProductQuery, ProductVariables>>
> {
  render() {
    return (
      <ReactApollo.Query<ProductQuery, ProductVariables>
        query={ProductDocument}
        {...(this as any)["props"] as any}
      />
    );
  }
}
export type ProductProps<TChildProps = any> = Partial<
  ReactApollo.DataProps<ProductQuery, ProductVariables>
> &
  TChildProps;
export function ProductHOC<TProps, TChildProps = any>(
  operationOptions:
    | ReactApollo.OperationOption<
        TProps,
        ProductQuery,
        ProductVariables,
        ProductProps<TChildProps>
      >
    | undefined
) {
  return ReactApollo.graphql<
    TProps,
    ProductQuery,
    ProductVariables,
    ProductProps<TChildProps>
  >(ProductDocument, operationOptions);
}
export function useProduct(
  baseOptions?: ReactApolloHooks.QueryHookOptions<ProductVariables>
) {
  return ReactApolloHooks.useQuery<ProductQuery, ProductVariables>(
    ProductDocument,
    baseOptions
  );
}
