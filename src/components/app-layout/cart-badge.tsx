import { Badge, Icon } from "antd";
import React from "react";
import { Link } from "src/server/routes";

const CartBadge = ({ quantity, isMobile = false }) => (
  <Link route="order" href="order" as="/order">
    <Badge count={quantity}>
      <Icon
        type="shopping-cart"
        style={{ color: isMobile ? "#111111" : "inherit" }}
      />
    </Badge>
  </Link>
);

export default CartBadge;
