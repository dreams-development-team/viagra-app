import styled from "@emotion/styled";

/**
 * AppLayout styles
 */

export const AppLayoutWrapper = styled.section({
  display: "flex",
  flexDirection: "column",
  justifyContent: "space-between",
  minHeight: "100vh"
});

export const AppLayoutHeader = styled.header(p => {
  return {
    display: "flex",
    justifyContent: "space-between",
    height: 120,
    width: "90vw",
    alignItems: "center",
    margin: "0 auto 45px auto",

    [p.theme.mq.xl]: {
      width: "80vw"
    }
  };
});

export const AppLayoutMain = styled.main({
  flexGrow: 1,
  marginBottom: 100
});

/**
 * Cart styles
 */

export const CartDesktopWrapper = styled.div(p => {
  return {
    width: 35,
    cursor: "pointer",

    [p.theme.mq.xs]: {
      display: "none"
    },

    [p.theme.mq.sm]: {
      display: "none"
    },

    [p.theme.mq.xl]: {
      display: "block"
    }
  };
});

export const MobileCart = styled.div(p => ({
  position: "fixed",
  bottom: 15,
  right: 15,
  height: 40,
  backgroundColor: "#63c900",
  width: 40,
  borderRadius: "50%",
  zIndex: 2,
  display: "flex",
  alignItems: "center",
  justifyContent: "center",

  [p.theme.mq.xl]: {
    display: "none"
  }
}));

/**
 * Logo styles
 */

export const LogoWrapper = styled.div(p => ({
  cursor: "pointer",
  display: "flex",
  alignItems: "center",

  [p.theme.mq.sm]: {
    flexDirection: "column",
    alignItems: "initial"
  }
}));

export const LogoShapesWrapper = styled.div(p => {
  return {
    display: "flex",
    justifyContent: "space-between",
    marginBottom: 10,

    [p.theme.mq.xs]: {
      flexDirection: "column"
    }
  };
});

export const LogoBlueShape = styled.div(p => {
  return {
    display: "inline-block",
    width: 80,
    height: 23,
    background: "#003049",
    marginRight: 7,

    [p.theme.mq.xs]: {
      marginBottom: 5,
      width: 40
    }
  };
});

export const LogoRedShape = styled.div(p => ({
  display: "inline-block",
  width: 80,
  height: 23,
  background: "#D62828",

  [p.theme.mq.xs]: {
    width: 40
  }
}));

export const LogoTitle = styled.h2(p => {
  return {
    display: "flex",
    justifyContent: "space-between",
    textTransform: "uppercase",

    [p.theme.mq.xs]: {
      flexDirection: "column"
    }
  };
});

/**
 * Navigation
 */

export const DesktopNav = styled.nav(p => {
  return {
    [p.theme.mq.xs]: {
      display: "none"
    },

    [p.theme.mq.sm]: {
      display: "none"
    },

    [p.theme.mq.xl]: {
      display: "flex",
      justifyContent: "center",
      fontFamily: ["Merriweather"],

      a: {
        fontWeight: 500,
        padding: 10,
        textDecoration: "none",
        color: "#111111",
        letterSpacing: "1.7px",
        borderBottom: "3px solid #FFFCFC",

        "&:hover": {
          borderBottom: "3px solid #111111"
        },

        "&:not(:last-child)": {
          marginRight: 30
        }
      }
    }
  };
});

export const MobileNavButton = styled.img(p => ({
  width: 30,

  [p.theme.mq.xl]: {
    display: "none"
  }
}));

export const MobileListModal = styled.div((p: any) => ({
  display: p.isOpen ? "block" : "none",
  position: "fixed",
  width: "100%",
  height: "100%",
  backgroundColor: "#ccc",
  top: 0,
  right: 0,
  zIndex: 3,
  overflowY: "scroll",

  [p.theme.mq.sm]: {
    width: "30%"
  }
}));

export const CloseModal = styled.div({
  position: "absolute",
  top: 20,
  right: 20
});

export const MobileModalContent = styled.div({
  display: "flex",
  flexDirection: "column",
  alignItems: "center",
  justifyContent: "center",
  height: "100%",
  backgroundColor: "#123C69",

  a: {
    fontWeight: 500,
    padding: 10,
    textDecoration: "none",
    color: "#FFFCFC",
    letterSpacing: "1.7px"
  }
});
