import { Icon } from "antd";
import React, { useState } from "react";
import { compose } from "recompose";
import { Link } from "src/server/routes";
import { withNamespaces } from "src/utils";
import {
  CloseModal,
  DesktopNav,
  MobileListModal,
  MobileModalContent,
  MobileNavButton
} from "./styles";

const Links = ({
  onClick,
  t
}: {
  onClick?: () => void;
  t: (t: string) => any;
}) => (
  <>
    <Link route="/">
      <a onClick={() => onClick && onClick()}>{t("shop-label")}</a>
    </Link>
    <Link route="/contact-us">
      <a onClick={() => onClick && onClick()}>{t("contact-us-label")}</a>
    </Link>
    <Link route="/delivery">
      <a onClick={() => onClick && onClick()}>{t("delivery-label")}</a>
    </Link>
    <Link route="/faq">
      <a onClick={() => onClick && onClick()}>{t("faq-label")}</a>
    </Link>
  </>
);

type Props = {
  t: (t: string) => any;
};

const enhance = compose<Props, any>(withNamespaces("navigation"));

const Navigation = ({ t }: Props) => {
  const [isOpen, setOpen] = useState(false);

  return (
    <div>
      <DesktopNav>
        <Links t={t} />
      </DesktopNav>
      <MobileNavButton
        src="/static/images/menu.svg"
        onClick={() => setOpen(true)}
      />
      <MobileListModal isOpen={isOpen}>
        <CloseModal onClick={() => setOpen(false)}>
          <Icon type="close" style={{ color: "#FFFCFC" }} />
        </CloseModal>
        <MobileModalContent>
          <Links onClick={() => setOpen(false)} t={t} />
        </MobileModalContent>
      </MobileListModal>
    </div>
  );
};

export default enhance(Navigation);
