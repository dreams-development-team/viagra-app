import React from "react";
import { Link } from "src/server/routes";
import {
  LogoBlueShape,
  LogoRedShape,
  LogoShapesWrapper,
  LogoTitle,
  LogoWrapper
} from "./styles";

const Logo = () => (
  <Link route="index">
    <LogoWrapper>
      <LogoShapesWrapper>
        <LogoBlueShape />
        <LogoRedShape />
      </LogoShapesWrapper>
      <LogoTitle>
        <div style={{ color: "#D62828" }}>Energy</div>
        <div style={{ color: "#003049" }}>Boost</div>
      </LogoTitle>
    </LogoWrapper>
  </Link>
);

export default Logo;
