import React from "react";
import CartBadge from "./cart-badge";
import { CartDesktopWrapper, MobileCart } from "./styles";

const Cart = ({ quantity }) => {
  return (
    <>
      <CartDesktopWrapper>
        <CartBadge quantity={quantity} />
      </CartDesktopWrapper>
      {Boolean(quantity) && (
        <MobileCart>
          <CartBadge quantity={quantity} isMobile={true} />
        </MobileCart>
      )}
    </>
  );
};

export default Cart;
