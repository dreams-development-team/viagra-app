import React from "react";
import { compose } from "recompose";
import { withOrder } from "src/hoc";
import { OrderStore } from "src/store/order";
import CatalogLayout from "../catalog-layout/catalog-layout";
import Cart from "./cart";
import Logo from "./logo";
import Navigation from "./navigation";
import { AppLayoutHeader, AppLayoutMain, AppLayoutWrapper } from "./styles";

type OProps = {
  children: React.ReactNode;
};

type IProps = OProps & {
  order: OrderStore;
};

const enhance = compose<IProps, OProps>(withOrder);

const AppLayout = ({ children, order }: IProps) => {
  return (
    <AppLayoutWrapper>
      <AppLayoutHeader>
        <Logo />
        <Navigation />
        <Cart
          quantity={order.state.cart.reduce((len, d) => len + d.quantity, 0)}
        />
      </AppLayoutHeader>
      <AppLayoutMain>
        <CatalogLayout>{children}</CatalogLayout>
      </AppLayoutMain>
    </AppLayoutWrapper>
  );
};

export default enhance(AppLayout);
