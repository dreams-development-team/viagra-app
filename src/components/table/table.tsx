import { Table as AntTable } from "antd";
import { TableProps as AntProps } from "antd/lib/table";
import React from "react";

type Props = AntProps<any> & {
  meta?: {
    count?: number;
    offset?: number;
    limit?: number;
  };
};

const Table = (props: Props) => {
  return <AntTable {...props} />;
};

export default Table;
