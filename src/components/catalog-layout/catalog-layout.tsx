import styled from "@emotion/styled";
import React from "react";
import { compose } from "recompose";
import { withNamespaces } from "src/utils";
import { useSearchProducts } from "../apollo-components";
import List from "./list";

const LayoutWrapper = styled.section(p => {
  return {
    display: "flex",
    margin: "0 auto",
    width: "90vw",

    [`${p.theme.mq.xs}`]: {
      flexDirection: "column"
    },

    [`${p.theme.mq.sm}`]: {
      flexDirection: "column"
    },

    [`${p.theme.mq.xl}`]: {
      flexDirection: "row",
      width: "80vw"
    }
  };
});

const Title = styled.h3(p => ({
  color: "#627A3D",
  letterSpacing: "2px",
  fontWeight: 600,
  textTransform: "uppercase",

  [`${p.theme.mq.xs}`]: {
    display: "none"
  },

  [`${p.theme.mq.sm}`]: {
    marginBottom: 5
  },

  [`${p.theme.mq.xl}`]: {
    marginBottom: 20
  }
}));

const Categories = styled.nav(p => {
  return {
    [`${p.theme.mq.md}`]: {
      marginBottom: 10,
      display: "block"
    },

    [`${p.theme.mq.lg}`]: {
      paddingRight: 50,
      marginBottom: 15
    }
  };
});

const Content = styled.section({
  flexBasis: "80%",
  flexGrow: 0,
  flexShrink: 0
});

type OProps = {
  children: React.ReactNode;
};

type IProps = OProps & {
  t: (t: string) => any;
};

const enhance = compose<IProps, OProps>(withNamespaces(["catalog-layout"]));

const CatalogLayout = ({ children, t }: IProps) => {
  const { data } = useSearchProducts();

  return (
    <LayoutWrapper>
      <Categories>
        <Title>{t("categories-label")}</Title>
        <List
          products={
            (data && data.searchProducts && data.searchProducts.data) || []
          }
        />
      </Categories>
      <Content>{children}</Content>
    </LayoutWrapper>
  );
};

export default enhance(CatalogLayout);
