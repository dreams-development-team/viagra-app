import styled from "@emotion/styled";
import { Icon } from "antd";
import { isEmpty } from "lodash";
import React, { useState } from "react";
import { compose } from "recompose";
import { SearchProductsData } from "src/components/apollo-components";
import { Link } from "src/server/routes";
import { withNamespaces } from "src/utils";

const Navigation = styled.nav(p => {
  return {
    display: "flex",
    flexDirection: "row",
    flexBasis: "20%",
    flexGrow: 0,
    flexShrink: 0,
    fontWeight: 500,
    justifyContent: "space-between",

    [`${p.theme.mq.xs}`]: {
      display: "none"
    },

    [p.theme.mq.md]: {
      marginBottom: 15
    },

    [`${p.theme.mq.xl}`]: {
      flexDirection: "column",
      justifyContent: "flex-start",
      marginBottom: 0
    }
  };
});

const AllLink = styled.a(p => {
  return {
    padding: "10px 0",
    textDecoration: "none",
    color: "#012575",
    letterSpacing: "1.7px",

    "&:hover": {
      color: "#D62828"
    },

    [p.theme.mq.xs]: {
      color: "#FFFCFC"
    }
  };
});

const MobileNavigation = styled.div(p => ({
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  marginBottom: 30,
  backgroundColor: "#123C69",
  color: "#FFFCFC",
  padding: 10,
  letterSpacing: 2,

  [p.theme.mq.sm]: {
    display: "none"
  }
}));

const ListModal = styled.div((p: any) => ({
  display: p.isOpen ? "block" : "none",
  position: "fixed",
  width: "100%",
  height: "100%",
  top: 0,
  right: 0,
  zIndex: 3,
  overflowY: "scroll"
}));

const CloseModal = styled.div({
  position: "absolute",
  top: 25,
  right: 25
});

const ModalContent = styled.div({
  display: "flex",
  flexDirection: "column",
  alignItems: "center",
  justifyContent: "center",
  height: "100%",
  backgroundColor: "#123C69",
  fontFamily: ["Raleway"],
  fontWeight: 500
});

type OProps = {
  products: SearchProductsData[];
};

type IProps = OProps & {
  t: (t: string) => any;
};

const enhance = compose<IProps, OProps>(withNamespaces(["catalog-layout"]));

const List = ({ products: products, t }: IProps) => {
  const [isOpen, setOpen] = useState(false);

  if (isEmpty(products)) {
    return <div>{t("empty-catalog")}</div>;
  }

  return (
    <div>
      <Navigation>
        <Link route="index">
          <AllLink>{t("all-products-label")}</AllLink>
        </Link>
        {products.map(p => (
          <Link key={p.id} route="index" params={{ url: p.url }}>
            <AllLink>{p.title}</AllLink>
          </Link>
        ))}
      </Navigation>
      <MobileNavigation onClick={() => setOpen(true)}>
        <Icon type="appstore" style={{ marginRight: 10, color: "#FFFCFC" }} />
        <span>{t("categories-label")}</span>
      </MobileNavigation>
      <ListModal isOpen={isOpen}>
        <CloseModal onClick={() => setOpen(false)}>
          <Icon type="close" style={{ color: "#FFFCFC" }} />
        </CloseModal>
        <ModalContent>
          <Link route="index">
            <AllLink>{t("all-products-label")}</AllLink>
          </Link>
          {products.map(p => (
            <Link key={p.id} route="index" params={{ url: p.url }}>
              <AllLink onClick={() => setOpen(false)}>{p.title}</AllLink>
            </Link>
          ))}
        </ModalContent>
      </ListModal>
    </div>
  );
};

export default enhance(List);
