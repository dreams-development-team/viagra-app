import Head from "next/head";
import React from "react";
import { Link } from "src/server/routes";

type Props = {
  title?: string;
  children: React.ReactNode;
};

const Layout = ({ children, title = "This is the default title" }: Props) => (
  <div>
    <Head>
      <title>{title}</title>
      <meta charSet="utf-8" />
      <meta name="viewport" content="initial-scale=1.0, width=device-width" />
    </Head>
    <header>
      <nav>
        <Link route="index">
          <a>Home</a>
        </Link>{" "}
        |{" "}
        <Link route="about" params={{ type: "Query" }}>
          <a>About</a>
        </Link>{" "}
        |{" "}
      </nav>
    </header>
    {children}
  </div>
);

export default Layout;
