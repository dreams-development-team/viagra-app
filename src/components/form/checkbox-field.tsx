import { Checkbox } from "antd";
import { CheckboxProps } from "antd/lib/checkbox";
import { Field, FieldProps } from "formik";
import { get } from "lodash";
import React from "react";
import FormItem, { FormItemProps } from "./form-item";

const CheckboxComponent = ({
  field,
  form: { touched, errors },
  wrapperCol,
  labelCol,
  label,
  help,
  required,
  ...props
}: FieldProps & FormItemProps) => {
  const error = get(touched, field.name) && get(errors, field.name);

  return (
    <FormItem
      required={required}
      label={label}
      labelCol={labelCol}
      wrapperCol={wrapperCol}
      validateStatus={error ? "error" : ""}
      help={error || help}
    >
      <Checkbox {...field} {...props} checked={Boolean(field.value)} />
    </FormItem>
  );
};

type Props = CheckboxProps &
  React.DetailedHTMLProps<
    React.InputHTMLAttributes<HTMLInputElement>,
    HTMLInputElement
  > &
  FormItemProps;

const CheckboxField = (props: Props) => (
  <Field {...props} component={CheckboxComponent} />
);

export default CheckboxField;
