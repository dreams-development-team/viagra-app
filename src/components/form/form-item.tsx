import { Form } from "antd";
import { FormItemProps as AntProps } from "antd/lib/form";
import React, { FC } from "react";

export type FormItemProps = AntProps & {
  required?: boolean;
};

const AntFormItem = Form.Item;

const FormItem: FC<FormItemProps> = ({ required, ...rest }) => {
  let label = rest.label;

  if (required) {
    label = (
      <>
        <span style={{ color: "crimson" }}>*</span> {label}
      </>
    );
  }

  return (
    <AntFormItem {...rest} label={label}>
      {rest.children}
    </AntFormItem>
  );
};

export default FormItem;
