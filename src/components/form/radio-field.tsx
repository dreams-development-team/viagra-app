import { Radio } from "antd";
import { RadioProps } from "antd/lib/radio";
import RadioButton from "antd/lib/radio/radioButton";
import { Field, FieldProps } from "formik";
import { get } from "lodash";
import React from "react";
import FormItem, { FormItemProps } from "./form-item";

const { Group } = Radio;

const RadioGroupComponent = ({
  field,
  form: { touched, errors },
  wrapperCol,
  labelCol,
  label,
  help,
  required,
  children,
  ...props
}: FieldProps & FormItemProps & { children: React.ReactNode }) => {
  const error = get(touched, field.name) && get(errors, field.name);

  return (
    <FormItem
      required={required}
      label={label}
      labelCol={labelCol}
      wrapperCol={wrapperCol}
      validateStatus={error ? "error" : ""}
      help={error || help}
    >
      <Group {...field} {...props}>
        {children}
      </Group>
    </FormItem>
  );
};

type Props = RadioProps &
  React.DetailedHTMLProps<
    React.InputHTMLAttributes<HTMLInputElement>,
    HTMLInputElement
  > &
  FormItemProps;

const RadioGroupField = (props: Props) => (
  <Field {...props} component={RadioGroupComponent} />
);

RadioGroupField.Button = RadioButton;
RadioGroupField.Radio = Radio;

export default RadioGroupField;
