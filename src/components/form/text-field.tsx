import { Input } from "antd";
import { TextAreaProps } from "antd/lib/input";
import { Field, FieldProps } from "formik";
import { get } from "lodash";
import React from "react";
import FormItem, { FormItemProps } from "./form-item";

const { TextArea } = Input;

const TextComponent = ({
  field,
  form: { touched, errors },
  wrapperCol,
  labelCol,
  label,
  help,
  required,
  ...props
}: FieldProps & FormItemProps) => {
  const error = get(touched, field.name) && get(errors, field.name);

  return (
    <FormItem
      required={required}
      label={label}
      labelCol={labelCol}
      wrapperCol={wrapperCol}
      validateStatus={error ? "error" : ""}
      help={error || help}
    >
      <TextArea {...field} {...props} />
    </FormItem>
  );
};

type Props = TextAreaProps &
  React.DetailedHTMLProps<
    React.InputHTMLAttributes<HTMLTextAreaElement>,
    HTMLTextAreaElement
  > &
  FormItemProps;

const TextField = (props: Props) => (
  <Field {...props} component={TextComponent} />
);

export default TextField;
