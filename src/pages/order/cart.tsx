import styled from "@emotion/styled";
import { Avatar, Button, Card, InputNumber, List } from "antd";
import { isEmpty, isNil } from "lodash";
import numeral from "numeral";
import React from "react";
import { compose } from "recompose";
import { CartInput, ProductProduct } from "src/components/apollo-components";
import { OrderStore } from "src/store/order";
import { withNamespaces } from "src/utils";

const ItemsWrapper = styled.div(p => ({
  display: "flex",
  width: "100%",
  flexWrap: "wrap",

  [p.theme.mq.xs]: {
    alignItems: "center",
    flexDirection: "column"
  }
}));

const ItemTitle = styled.div({
  flexGrow: 1,
  marginBottom: 15
});

const ItemQuantity = styled.div(p => ({
  marginBottom: 10,

  [p.theme.mq.xm]: {
    marginRight: 10
  }
}));

const Total = styled.div({
  textAlign: "right"
});

type OProps = {
  cart: CartInput[];
  productsById?: {
    [id: number]: ProductProduct;
  };
  setQuantity: typeof OrderStore.prototype.setQuantity;
  removeFromCart: typeof OrderStore.prototype.removeFromCart;
};

type IProps = OProps & {
  t: (t: string) => any;
};

const enhance = compose<IProps, OProps>(withNamespaces(["order", "common"]));

const Cart = ({
  cart,
  productsById,
  t,
  setQuantity,
  removeFromCart
}: IProps) => {
  if (isEmpty(productsById) || isNil(productsById)) {
    return null;
  }

  const totalPrice = cart.reduce((acc, cartItem) => {
    const price = productsById[cartItem.productId].dosages
      .find(dosage => dosage.id === cartItem.dosageId)
      .details.find(detail => detail.id === cartItem.dosageDetailId).price;

    return acc + price * cartItem.quantity;
  }, 0);

  return (
    <Card title={t("cart-label")} style={{ marginBottom: 50 }}>
      <List
        itemLayout="horizontal"
        dataSource={cart}
        renderItem={cartItem => {
          const product = productsById[cartItem.productId];
          const dosage = product.dosages.find(d => d.id === cartItem.dosageId);
          const detail = dosage.details.find(
            d => d.id === cartItem.dosageDetailId
          );

          return (
            <List.Item>
              <ItemsWrapper>
                <ItemTitle>
                  <List.Item.Meta
                    avatar={<Avatar src={product.logo.url} />}
                    title={
                      <a href="https://ant.design">
                        {productsById[cartItem.productId].title}
                      </a>
                    }
                    description={`${dosage.dosage} X ${
                      detail.count
                    }, price: ${numeral(detail.price).format("$0,0.00")}`}
                  />
                </ItemTitle>
                <ItemQuantity>
                  {t("common:quantity-label")}:{" "}
                  <InputNumber
                    min={1}
                    value={cartItem.quantity}
                    onChange={v => {
                      setQuantity({
                        productId: cartItem.productId,
                        dosageDetailId: cartItem.dosageDetailId,
                        dosageId: cartItem.dosageId,
                        quantity: v
                      });
                    }}
                  />
                </ItemQuantity>
                <Button
                  onClick={() => {
                    removeFromCart({
                      productId: cartItem.productId,
                      dosageDetailId: cartItem.dosageDetailId,
                      dosageId: cartItem.dosageId
                    });
                  }}
                >
                  {t("common:remove-label")}
                </Button>
              </ItemsWrapper>
            </List.Item>
          );
        }}
      />
      <Total>
        <b>Total:</b> {numeral(totalPrice).format("$0,0.00")}
      </Total>
    </Card>
  );
};

export default enhance(Cart);
