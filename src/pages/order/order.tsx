import { Button, Empty, Modal } from "antd";
import { isEmpty, uniq } from "lodash";
import Router from "next/router";
import React, { useState } from "react";
import { compose } from "recompose";
import {
  CartInput,
  useCreateOrder,
  useSearchProducts
} from "src/components/apollo-components";
import { Center } from "src/components/helpers";
import { Spinner } from "src/components/spinner";
import { withOrder } from "src/hoc";
import { Link } from "src/server/routes";
import { OrderStore, orderStore } from "src/store/order";
import { validateFormSync, withNamespaces } from "src/utils";
import * as yup from "yup";
import Cart from "./cart";
import OrderForm from "./order-form";

type Props = {
  cart: CartInput[];
  t: (t: string, params?: any) => any;
  order: OrderStore;
};

const enhance = compose<Props, any>(
  withNamespaces(["order", "common"]),
  withOrder
);

const createOrderSchema = t =>
  yup.object({
    customerName: yup
      .string()
      .trim()
      .lowercase()
      .required(t("validation-required-name-label")),
    email: yup
      .string()
      .email()
      .trim()
      .lowercase()
      .required(t("validation-required-email-label")),
    address: yup
      .object({
        city: yup
          .string()
          .trim()
          .lowercase()
          .required(t("validation-required-city-label")),
        street: yup
          .string()
          .trim()
          .lowercase()
          .required(t("validation-required-street-label")),
        apartment: yup
          .string()
          .trim()
          .lowercase()
          .required(t("validation-required-apartment-label")),
        postalCode: yup
          .string()
          .trim()
          .lowercase()
          .required(t("validation-required-postal-code-label"))
      })
      .required(),
    phone: yup
      .string()
      .trim()
      .nullable(true),
    comment: yup
      .string()
      .trim()
      .nullable(true)
  });

const Order = ({ t, order }: Props) => {
  const [oLoading, setOLoading] = useState(false);
  const { data, loading: pLoading } = useSearchProducts({
    skip: isEmpty(order.state.cart),
    variables: { where: { id: uniq(order.state.cart.map(c => c.productId)) } }
  });
  const mutation = useCreateOrder();

  if (pLoading) return <Spinner />;

  if (isEmpty(order.state.cart)) {
    return (
      <Center>
        <Empty description={t("empty")}>
          <Link route="index">
            <Button type="primary" size="large">
              Shopping!
            </Button>
          </Link>
        </Empty>
      </Center>
    );
  }

  const productsById =
    data &&
    data.searchProducts &&
    data.searchProducts.data!.reduce((a, p) => ({ [p.id]: p, ...a }), {});

  return (
    <>
      <Cart
        cart={order.state.cart}
        productsById={productsById}
        setQuantity={orderStore.setQuantity.bind(orderStore)}
        removeFromCart={orderStore.removeFromCart.bind(orderStore)}
      />
      <OrderForm
        loading={oLoading}
        onSubmit={async (values: any, { setErrors }) => {
          const errors = validateFormSync({
            values,
            schema: createOrderSchema(t)
          });

          if (!isEmpty(errors)) {
            setErrors(errors);

            return;
          }

          try {
            setOLoading(true);

            const orderResult = await mutation({
              variables: {
                data: { ...values, cart: order.state.cart }
              }
            });

            Modal.success({
              title: t("success-create-order-message", {
                number: orderResult.data.createOrder.number.toLocaleUpperCase()
              }),
              content: (
                <span>
                  {t("check-email-message", {
                    email: values.email
                  })}
                </span>
              ),
              onOk: async () => {
                orderStore.clear();
                await Router.push("/");
              }
            });

            setOLoading(false);
          } catch {
            setOLoading(false);
          }
        }}
      />
    </>
  );
};

Order.getInitialProps = () => {
  return {
    namespacesRequired: ["order", "common"]
  };
};

export default enhance(Order);
