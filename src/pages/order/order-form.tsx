import { Button, Card, Col, Row } from "antd";
import { Form, Formik } from "formik";
import React from "react";
import { compose } from "recompose";
import {
  DeliveryStatus,
  DeliveryTypes,
  PaymentStatus,
  PaymentTypes
} from "src/components/apollo-components";
import { InputField, TextField } from "src/components/form";
import { withNamespaces } from "src/utils";

type OProps = {
  onSubmit: any;
  loading: boolean;
};

type IProps = OProps & {
  t: (t: string) => any;
};

const enhance = compose<IProps, OProps>(withNamespaces(["order", "common"]));

const OrderForm = ({ onSubmit, loading, t }: IProps) => {
  return (
    <Card title={t("details-label")}>
      <Formik
        initialValues={{
          customerName: "",
          email: "",
          address: {
            city: "",
            street: "",
            apartment: "",
            postalCode: ""
          },
          comment: "",
          deliveryType: DeliveryTypes.Courier,
          deliveryStatus: DeliveryStatus.Pending, // TODO: remove
          paymentType: PaymentTypes.Cash,
          paymentStatus: PaymentStatus.Pending // TODO: remove,
        }}
        onSubmit={onSubmit}
        render={() => (
          <Form>
            <Row gutter={20} type="flex">
              <Col xs={24} sm={8}>
                <InputField
                  name="customerName"
                  label={t("name-label")}
                  required={true}
                />
              </Col>

              <Col xs={24} sm={8}>
                <InputField
                  name="email"
                  label={t("email-label")}
                  required={true}
                />
              </Col>
              <Col xs={24} sm={8}>
                <InputField name="phone" label={t("phone-label")} />
              </Col>
              <Col xs={24} sm={24}>
                <Row gutter={20}>
                  <Col xs={24} sm={24}>
                    <h3>{t("address-label")}:</h3>
                  </Col>
                  <Col xs={24} sm={24} style={{ marginBottom: 24 }}>
                    {t("address-info")}
                  </Col>
                  <Col xs={24} sm={6}>
                    <InputField
                      name="address.city"
                      label={t("city-label")}
                      required={true}
                    />
                  </Col>
                  <Col xs={24} sm={6}>
                    <InputField
                      name="address.street"
                      label={t("street-label")}
                      required={true}
                    />
                  </Col>
                  <Col xs={24} sm={6}>
                    <InputField
                      name="address.apartment"
                      label={t("apartment-label")}
                      required={true}
                    />
                  </Col>
                  <Col xs={24} sm={6}>
                    <InputField
                      name="address.postalCode"
                      label={t("postal-code-label")}
                      required={true}
                    />
                  </Col>
                </Row>
              </Col>
              <Col xs={24} sm={24}>
                <TextField name="comment" label={t("comment-label")} rows={5} />
              </Col>
            </Row>
            <Button
              style={{ float: "right" }}
              type="primary"
              size="large"
              htmlType="submit"
              disabled={loading}
              loading={loading}
            >
              {loading ? t("common:loading-label") : t("checkout-label")}
            </Button>
          </Form>
        )}
      />
    </Card>
  );
};

export default enhance(OrderForm);
