import { NormalizedCacheObject } from "apollo-cache-inmemory";
import { ApolloClient } from "apollo-client";
import cookieParser from "cookie-parse";
import { NextContext } from "next";
import App, { Container } from "next/app";
import Head from "next/head";
import React from "react";
import { ApolloProvider } from "react-apollo";
import { ApolloProvider as ApolloHooksProvider } from "react-apollo-hooks";
import { compose } from "recompose";
import AppLayout from "src/components/app-layout";
import ThemeLayout from "src/components/theme-layout";
import { orderStore } from "src/store/order";
import { appWithTranslation, withApolloClient } from "src/utils";
import { Provider } from "unstated";

const enhance = compose(
  withApolloClient,
  appWithTranslation
);

class MyApp extends App<{ apolloClient: ApolloClient<NormalizedCacheObject> }> {
  static async getInitialProps({
    Component,
    ctx
  }: {
    Component: any;
    ctx: NextContext;
  }) {
    let pageProps = {};

    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps(ctx);
    }

    if (ctx && ctx.req && ctx.req.headers.cookie) {
      const cookie = cookieParser.parse(ctx.req.headers.cookie);

      cookie.order &&
        (await orderStore.setInitialState(
          JSON.parse(cookie.order) || { cart: [] }
        ));
    }

    return { pageProps };
  }

  render() {
    const { Component, pageProps, apolloClient } = this.props;

    return (
      <Container>
        <Head>
          <meta name="viewport" content="width=device-width, initial-scale=1" />
          <meta charSet="utf-8" />
          <link rel="icon" type="image/x-icon" href="/static/favicon.ico" />
          <link
            href="https://fonts.googleapis.com/css?family=Merriweather:300,300i,400,400i,700,700i,900,900i&amp;subset=cyrillic,cyrillic-ext,latin-ext,vietnamese"
            rel="stylesheet"
          />
          <link
            href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
            rel="stylesheet"
          />
        </Head>
        <ApolloProvider client={apolloClient}>
          <ApolloHooksProvider client={apolloClient}>
            <ThemeLayout>
              <Provider inject={[orderStore]}>
                <AppLayout>
                  <Component {...pageProps} />
                </AppLayout>
              </Provider>
            </ThemeLayout>
          </ApolloHooksProvider>
        </ApolloProvider>
      </Container>
    );
  }
}

export default enhance(MyApp);
