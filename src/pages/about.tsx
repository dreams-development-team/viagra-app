import Link from "next/link";
import React from "react";
import Layout from "../components/layout";

type Props = {
  type?: string;
};

const About = ({ type }: Props) => (
  <Layout title="About | Next.js + TypeScript Example">
    <h1>About</h1>
    {Boolean(type) && <h2>Type: {type}</h2>}
    <p>This is the about page</p>
    <p>
      <Link href="/">
        <a>Go home</a>
      </Link>
    </p>
  </Layout>
);

About.getInitialProps = ({ query }) => {
  return { type: query.type };
};

export default About;
