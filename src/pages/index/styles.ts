import styled from "@emotion/styled";

export const HomeTitle = styled.h2({
  margin: "0 0 30px 0",
  color: "#123C69",
  letterSpacing: "2px",
  fontWeight: 600,
  textTransform: "uppercase"
});
