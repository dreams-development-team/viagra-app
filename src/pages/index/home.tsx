import { isEmpty } from "lodash";
import NextSeo from "next-seo";
import Head from "next/head";
import React from "react";
import { compose } from "recompose";
import {
  useProduct,
  useSearchProducts
} from "src/components/apollo-components";
import { Spinner } from "src/components/spinner";
import { withNamespaces } from "src/utils";
import Catalog from "./catalog/catalog";
import Product from "./product/product";
import { HomeTitle } from "./styles";

type Props = {
  t: (t: string) => any;
  query: { url?: string };
};

const enhance = compose<Props, any>(withNamespaces("index"));

const Home = ({ query, t }: Props) => {
  const { data: productsData, loading: productsLoading } = useSearchProducts({
    skip: !isEmpty(query)
  });
  const { data: productData, loading: productLoading } = useProduct({
    skip: isEmpty(query),
    variables: { where: { url: query.url } }
  });

  if (productLoading || productsLoading) {
    return <Spinner />;
  }

  // TODO: handle not found product and show default seo

  return (
    <>
      <NextSeo
        config={{
          title:
            !isEmpty(query) && productData && productData.product
              ? productData.product.seoTitle
              : t("seo-title"),
          description:
            !isEmpty(query) && productData && productData.product
              ? productData.product.seoDescription
              : t("seo-description")
        }}
      />
      <Head>
        <meta
          name="keywords"
          content={
            !isEmpty(query) && productData && productData.product
              ? productData.product.seoKeywords.join(", ")
              : t("seo-keywords")
          }
        />
      </Head>
      <HomeTitle>
        {!isEmpty(query) ? productData.product.title : t("catalog-label")}
      </HomeTitle>
      {isEmpty(query) ? (
        <Catalog products={productsData.searchProducts.data} />
      ) : (
        <Product product={productData.product} />
      )}
    </>
  );
};

Home.getInitialProps = ({ query }) => ({
  query,
  namespacesRequired: ["index", "catalog-layout", "navigation", "product"]
});

export default enhance(Home);
