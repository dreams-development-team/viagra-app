import css from "@emotion/css";
import styled from "@emotion/styled";
import { Col, Row } from "antd";
import React from "react";
import { compose } from "recompose";
import { withNamespaces } from "src/utils";

const restrictionCss = css({
  "> div:not(:last-child)": {
    marginBottom: 15
  }
});

const Image = styled.img(p => {
  return {
    objectFit: "cover",
    width: "10%",
    height: "auto",
    marginRight: 10,

    [p.theme.mq.xs]: {
      width: "13%"
    }
  };
});

const RestrictionDescription = styled.div({
  display: "inline-block"
});
const RestrictionValue = styled.span({ fontWeight: 500 });

type OProps = {
  withAlcohol: boolean;
  timeToStart: string;
  workTime: string;
};

type IProps = OProps & {
  t: (t: string, params?: any) => any;
};

const enhance = compose<IProps, OProps>(withNamespaces(["product", "common"]));

const Restrictions = ({ withAlcohol, timeToStart, workTime, t }: IProps) => {
  return (
    <Row gutter={10} type="flex" justify="center" css={restrictionCss}>
      <Col xs={24} sm={24}>
        <Image
          src={
            withAlcohol
              ? "/static/images/champagne.svg"
              : "/static/images/no-alcohol.svg"
          }
          alt={t("with-alcohol-label")}
        />
        <RestrictionDescription>
          <span>{t("with-alcohol-label")}: </span>
          <RestrictionValue>
            {withAlcohol ? t("common:yes-label") : t("common:no-label")}
          </RestrictionValue>
        </RestrictionDescription>
      </Col>
      <Col xs={24} sm={24}>
        <Image
          src={"/static/images/stopclock.svg"}
          alt={t("time-to-work-label")}
        />
        <RestrictionDescription>
          <span>{t("time-to-work-label")}: </span>
          <RestrictionValue>{timeToStart}</RestrictionValue>
        </RestrictionDescription>
      </Col>
      <Col xs={24} sm={24}>
        <Image src={"/static/images/clock.svg"} alt={t("work-time-label")} />
        <RestrictionDescription>
          <span>{t("work-time-label")}: </span>
          <RestrictionValue>{workTime}</RestrictionValue>
        </RestrictionDescription>
      </Col>
    </Row>
  );
};

export default enhance(Restrictions);
