import css from "@emotion/css";
import { Col, Row } from "antd";
import React from "react";
import { ProductProduct } from "src/components/apollo-components";
import Dosages from "./dosages";
import Meta from "./meta";

const descriptionCss = css({
  marginBottom: 50
});

type Props = {
  product: ProductProduct;
};

const Product = ({ product }: Props) => {
  return (
    <Row gutter={35}>
      <Col xs={24} sm={24}>
        <Meta
          title={product.title}
          logoUrl={product.logo.url}
          meta={product.meta}
          withAlcohol={product.withAlcohol}
          timeToStart={product.timeToStart}
          workTime={product.workTime}
        />
      </Col>
      <Col xs={24} sm={24} css={descriptionCss}>
        {product.description}
      </Col>
      <Col xs={24} sm={24}>
        <Dosages product={product} />
      </Col>
    </Row>
  );
};

export default Product;
