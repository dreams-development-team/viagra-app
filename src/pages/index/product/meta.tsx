import css from "@emotion/css";
import styled from "@emotion/styled";
import { Col, Row } from "antd";
import React from "react";
import { useMedia } from "react-use";
import { MOBILE_SIZE } from "src/theme/constants";
import Restrictions from "./restrictions";

const wrapperCss = css({
  marginBottom: 45
});

const titleClass = theme =>
  css({
    [theme.mq.xs]: {
      marginBottom: 20
    }
  });

const Logo = styled.img({
  border: "1px solid #123C69",
  objectFit: "cover",
  width: "100%",
  height: "auto",
  marginBottom: 20
});

const metaCss = theme =>
  css({
    [theme.mq.xs]: {
      marginBottom: 40
    }
  });

const Meta = ({ logoUrl, title, meta, withAlcohol, timeToStart, workTime }) => {
  const isMobile = useMedia(MOBILE_SIZE);

  return (
    <Row gutter={20} css={wrapperCss}>
      {isMobile && (
        <Col xs={24}>
          <h3 css={titleClass}>{title}</h3>
        </Col>
      )}
      <Col xs={24} sm={8}>
        <Logo src={logoUrl} alt={title} />
      </Col>
      <Col xs={24} sm={16}>
        {!isMobile && <h3 css={titleClass}>{title}</h3>}
        <Row gutter={15}>
          <Col xs={24} sm={12} css={metaCss}>
            {meta.map((m, i) => (
              <div key={i}>
                {m.key}: <b>{m.value}</b>
              </div>
            ))}
          </Col>
          <Col xs={24} sm={12}>
            <Restrictions
              withAlcohol={withAlcohol}
              timeToStart={timeToStart}
              workTime={workTime}
            />
          </Col>
        </Row>
      </Col>
    </Row>
  );
};

export default Meta;
