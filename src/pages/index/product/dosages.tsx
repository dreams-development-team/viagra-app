import css from "@emotion/css";
import { Button, message } from "antd";
import { ColumnProps } from "antd/lib/table";
import numeral from "numeral";
import React from "react";
import { compose } from "recompose";
import {
  ProductDetails,
  ProductDosages,
  ProductProduct
} from "src/components/apollo-components";
import Table from "src/components/table";
import { CartPayload, OrderStore } from "src/store/order";
import { withNamespaces } from "src/utils";
import { Subscribe } from "unstated";

const tableCss = theme =>
  css({
    marginBottom: 24,
    [theme.mq.xs]: {
      "& *": {
        fontSize: "1rem !important"
      }
    }
  });

type OProps = {
  product: ProductProduct;
};

type IProps = OProps & {
  t: (t: string, params: any) => any;
};

const enhance = compose<IProps, OProps>(withNamespaces(["product", "common"]));

const Dosages = ({ product, t }: IProps) => {
  return (
    <Subscribe to={[OrderStore]}>
      {(order: OrderStore) =>
        product.dosages.map(dosage => (
          <Table
            key={dosage.id}
            css={tableCss}
            pagination={false}
            bordered={true}
            rowKey="id"
            dataSource={dosage.details}
            columns={getColumns({
              addToCart: (p: CartPayload) => order.addToCart(p),
              dosage,
              product,
              t
            })}
          />
        ))
      }
    </Subscribe>
  );
};

function getColumns({
  product,
  dosage,
  t,
  addToCart
}: {
  product: ProductProduct;
  dosage: ProductDosages;
  t: (...t: any) => string;
  addToCart: typeof OrderStore.prototype.addToCart;
}): Array<ColumnProps<ProductDetails>> {
  return [
    {
      title: t("common:package-label"),
      key: "package",
      render: (_, { count }) => {
        return `${dosage.dosage} X ${count}`;
      }
    },
    {
      title: t("common:price-label"),
      key: "price",
      dataIndex: "price",
      render: value => numeral(value).format("$0,0.00")
    },
    {
      title: t("add-to-card-label"),
      key: "add-to-card",
      align: "center",
      render: (_, { id }) => (
        <Button
          onClick={() => {
            addToCart({
              productId: product.id,
              dosageDetailId: id,
              dosageId: dosage.id
            });

            message.success(t("add-to-card-message", { title: product.title }));
          }}
        >
          {t("add-to-card-label")}
        </Button>
      )
    }
  ];
}

export default enhance(Dosages);
