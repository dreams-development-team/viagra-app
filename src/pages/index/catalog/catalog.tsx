import { Col, Empty, Row } from "antd";
import { isEmpty } from "lodash";
import React from "react";
import { withNamespaces } from "react-i18next";
import { compose } from "recompose";
import { SearchProductsData } from "src/components/apollo-components";
import { Center } from "src/components/helpers";
import Product from "./catalog-product";

type OProps = {
  products: SearchProductsData[];
};

type IProps = OProps & {
  t: (t: string) => string;
};

const enhance = compose<IProps, OProps>(withNamespaces("index"));

const Catalog = ({ products, t }: IProps) => {
  return (
    <Row gutter={35} type="flex">
      <Col xs={24} sm={24}>
        {isEmpty(products) && (
          <Center>
            <Empty description={t("empty-products")} />
          </Center>
        )}
      </Col>
      {products.map(p => (
        <Col key={p.id} xs={24} sm={8}>
          <Product product={p} />
        </Col>
      ))}
    </Row>
  );
};

export default enhance(Catalog);
