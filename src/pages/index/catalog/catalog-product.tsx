import numeral from "numeral";
import React from "react";
import { compose } from "recompose";
import { ProductProduct } from "src/components/apollo-components";
import { Link } from "src/server/routes";
import { withNamespaces } from "src/utils";
import { BuyButton, Details, Info, Logo, Wrapper } from "./styles";

type OProps = {
  product: ProductProduct;
};

type IProps = OProps & {
  t: (t: string) => any;
};

const enhance = compose<IProps, OProps>(withNamespaces(["index"]));

const Product = ({ product, t }: IProps) => {
  return (
    <Link
      key={product.id}
      route="index"
      params={{
        url: product.url
      }}
    >
      <Wrapper>
        <Logo src={product.logo.url} alt={product.title} />
        <Info>
          <Details>
            <h3 className="product-name">{product.title}</h3>
            <div className="price">
              {t("from-price-label")}:{" "}
              <span style={{ color: "#0FB10D" }}>
                {numeral(23.23).format("$0,0.00")}
              </span>
            </div>
          </Details>
          <BuyButton>{t("buy-label")}</BuyButton>
        </Info>
      </Wrapper>
    </Link>
  );
};

export default enhance(Product);
