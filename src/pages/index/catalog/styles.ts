import styled from "@emotion/styled";

export const Wrapper = styled.div({
  display: "flex",
  flexDirection: "column",
  justifyContent: "space-between",
  cursor: "pointer",
  fontWeight: 300,
  height: "100%"
});

export const Logo = styled.img({
  objectFit: "cover",
  width: "100%",
  height: "auto",
  marginBottom: 20
});

export const Info = styled.div({
  height: "100%",
  display: "flex",
  flexDirection: "column",
  marginBottom: 60
});

export const BuyButton = styled.a({
  color: "#303c6c",
  textTransform: "uppercase",
  textAlign: "right",
  fontWeight: 500,
  fontFamily: ["Raleway"],

  "&:hover": {
    color: "#F3D250"
  }
});

export const Details = styled.div(p => ({
  display: "flex",
  flexGrow: 1,
  flexWrap: "wrap",

  flexDirection: "column",
  borderBottom: "2px solid",
  justifyContent: "flex-start",

  "& .product-name": {
    marginBottom: 5
  },

  [p.theme.mq.lg]: {
    flexDirection: "row",
    justifyContent: "space-between"
  }
}));
