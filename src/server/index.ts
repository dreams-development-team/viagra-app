import * as dotenv from "dotenv";
import express from "express";
import next from "next";
import nextI18NextMiddleware from "next-i18next/middleware";
import NextI18NextInstance from "../utils/i18n";
import routes from "./routes";

dotenv.config();

const app = next({ dev: process.env.NODE_ENV !== "production", dir: "src" });
const port = process.env.PORT || 3000;
const handler = routes.getRequestHandler(app);

(async () => {
  await app.prepare();

  const server = express();

  server.use(nextI18NextMiddleware(NextI18NextInstance));

  server.get("*", handler);

  server.listen(port, () => {
    console.log(`> Ready on http://localhost:${port}`);
  });
})().catch(e => {
  throw e;
});
