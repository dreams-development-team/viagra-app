import NextRouter from "next-routes";

export const routes = new NextRouter();
export const { Router, Link } = routes;

routes.add("order", "/order");
routes.add("index", "/:url?");

export default routes;
