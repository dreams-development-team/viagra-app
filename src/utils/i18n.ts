import NextI18Next from "next-i18next";

const NextI18NextInstance = new NextI18Next({
  defaultLanguage: "en",
  otherLanguages: ["en"],
  localePath: "src/static/locales",
  serverLanguageDetection: false,
  browserLanguageDetection: false
});

export default NextI18NextInstance;

export const { appWithTranslation, withNamespaces, i18n } = NextI18NextInstance;
