import { InMemoryCache, NormalizedCacheObject } from "apollo-cache-inmemory";
import { ApolloClient } from "apollo-client";
import { ApolloLink } from "apollo-link";
import { setContext } from "apollo-link-context";
import { onError } from "apollo-link-error";
import { createUploadLink } from "apollo-upload-client";
import fetch from "isomorphic-unfetch";
import store from "store";

export const isBrowser = Boolean((process as any).browser);

let apolloClient: ApolloClient<NormalizedCacheObject>;

if (!isBrowser) {
  (global as any).fetch = fetch;
}

function create(initialState) {
  const authLink = setContext((_, { headers }) => {
    const token = store.get("token");
    const _headers = { ...headers };

    if (token) {
      _headers.Authorization = `Bearer ${token}`;
    }

    return {
      headers: _headers
    };
  });

  return new ApolloClient({
    connectToDevTools: isBrowser,
    ssrMode: !isBrowser,
    link: ApolloLink.from([
      onError(({ graphQLErrors, networkError }) => {
        if (graphQLErrors) {
          graphQLErrors.map(({ message, locations, path }) => {
            console.error(
              `[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`
            );
          });
        }

        if (networkError) {
          console.error(`[Network error]: ${networkError}`);
        }
      }),
      authLink,
      createUploadLink({
        uri: process.env.API_URI,
        credentials: "same-origin"
      })
    ]),
    cache: new InMemoryCache().restore(initialState || {})
  });
}

export default function initApollo(initialState?: any) {
  if (!isBrowser) {
    return create(initialState);
  }

  if (!apolloClient) {
    apolloClient = create(initialState);
  }

  return apolloClient;
}
