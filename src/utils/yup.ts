import { set } from "lodash";
import { MixedSchema, ValidateOptions, ValidationError } from "yup";

type MyValidateOptions = {
  schema: MixedSchema;
  values: any;
  config?: ValidateOptions;
};

export function yupToFormErrors(yupError: ValidationError) {
  let errors: { [path: string]: string } = {};

  if (yupError.inner.length === 0) {
    return set(errors, yupError.path, yupError.message);
  }

  for (const err of yupError.inner) {
    if (!errors[err.path]) {
      errors = set(errors, err.path, err.message);
    }
  }

  return errors;
}

export async function validateFormAsync({
  schema,
  values,
  config
}: MyValidateOptions) {
  try {
    await schema.validate(values, { abortEarly: false, ...config });
  } catch (e) {
    throw yupToFormErrors(e);
  }
}

export function validateFormSync({
  schema,
  values,
  config
}: MyValidateOptions) {
  try {
    schema.validateSync(values, { abortEarly: false, ...config });

    return {};
  } catch (e) {
    return yupToFormErrors(e);
  }
}
